name := "SolidFire-API-Descriptors"

exportJars := true

fork in run := true

logLevel := Level.Info

testOptions += Tests.Argument( TestFrameworks.JUnit, "-q", "-v" )

lazy val sdkProject = (project in file( "." )).
  settings( Config.settings: _* ).
  aggregate(
    elementApi
  )


lazy val elementApi = Project(
  id = "element-api",
  base = file( "element-api" ),
  settings = Config.settings ++ Seq(
    description := "Common functions for processing SolidFire API descriptors.",
    libraryDependencies ++= Seq(
      Dependencies.jodaConvert,
      Dependencies.jodaTime,
      Dependencies.liftweb,
      Dependencies.jsvcgen
    ),
    mainClass in Compile := Some("com.solidfire.apigen.ApiMerge")
  )
)

packageOptions in(Compile, packageBin) += Package.ManifestAttributes(
  java.util.jar.Attributes.Name.IMPLEMENTATION_VERSION -> version.value
)

import sbt.Keys._
import sbt._

object Config {
  lazy val javaCompilerOptions = Seq(
    "-source", Version.javaLanguage,
    "-target", Version.javaTarget,
    "-Xlint"
  )

  lazy val javadocOptions = Seq( "-Xdoclint:none" )

  lazy val scalaCompilerOptions = Seq(
    "-deprecation",
    "-encoding", "UTF-8", // yes, this is 2 args
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-unchecked",
    "-Xfatal-warnings",
    "-Xlint",
    "-Yno-adapted-args",
    "-Ywarn-dead-code", // N.B. doesn't work well with the ??? hole
    "-Ywarn-numeric-widen",
    "-Ywarn-value-discard",
    "-Xfuture"
  )

  lazy val org = "com.solidfire"

  lazy val orgName = "SolidFire, Inc."

  lazy val artifactoryHost = "bdr-web-repo.eng.solidfire.net"

  def artifactory( contextPath: String ) = s"http://$artifactoryHost:8081/artifactory/$contextPath"

  lazy val settings = Defaults.coreDefaultSettings ++ Seq(
    javacOptions ++= javaCompilerOptions,
    javacOptions in doc := javadocOptions,
    scalacOptions ++= scalaCompilerOptions,
    scalaVersion := "2.10.5",
    version := Version.solidfireSdks,
    organization := org,
    resolvers := repositories,
    crossPaths := false, // do not append _${scalaVersion} to generated JAR
    autoScalaLibrary := false, // do not add Scala libraries as a dependency
    unmanagedSourceDirectories in Compile += baseDirectory.value / "src/generated/java",
    unmanagedSourceDirectories in Compile += baseDirectory.value / "src/generated/scala",
    libraryDependencies ++= Seq(
      Dependencies.slf4j_simple,
      Dependencies.json4sCore,
      Dependencies.json4sJackson,
      Dependencies.scalatest    % "test",
      Dependencies.scalacheck   % "test"
    )
  )

  lazy val repositories = List(
    "SolidFire Maven Repo" at artifactory( "ivy-release-local" ),
    "Maven Central" at "http://repo1.maven.org/maven2/"
  )

  lazy val junitReports = testOptions in Test <+= (target in Test) map { target =>
    val reportTarget = target / "test-reports"
    Tests.Argument( TestFrameworks.ScalaTest, s"""junitxml(directory="$reportTarget")""" )
  }
}

object Version {
  //this project
  val solidfireSdks = "8.0.17-SNAPSHOT"
//
  val javaLanguage  = "1.7"
  val javaTarget    = "1.7"

  val json4s     = "3.2.11"
  val liftweb       = "2.6.2"
  val jodaConvert   = "1.2"
  val jodaTime      = "2.4"
  val jsvcgen       = "0.1.8"

  val scalatest     = "2.2.2"
  val junit         = "0.11"
  val mockito       = "1.9.5"
  val scalacheck    = "1.12.2"
  val logback       = "1.1.3"
  val slf4j         = "1.7.12"
}

object Dependencies {
  lazy val json4sCore    = "org.json4s"               %% "json4s-core"      % Version.json4s
  lazy val json4sJackson = "org.json4s"               %% "json4s-jackson"   % Version.json4s
  lazy val liftweb      = "net.liftweb"               %% "lift-json"        % Version.liftweb
  lazy val jodaTime     = "joda-time"                 % "joda-time"         % Version.jodaTime
  lazy val jodaConvert  = "org.joda"                  % "joda-convert"      % Version.jodaConvert
  lazy val jsvcgen      = "com.solidfire"             %% "jsvcgen"          % Version.jsvcgen

  lazy val scalatest    = "org.scalatest"             %% "scalatest"        % Version.scalatest
  lazy val mockito      = "org.mockito"               % "mockito-all"       % Version.mockito
  lazy val scalacheck   = "org.scalacheck"            %% "scalacheck"       % Version.scalacheck
  lazy val logback      = "ch.qos.logback"            % "logback-classic"   % Version.logback
  lazy val slf4j_simple = "org.slf4j"                 % "slf4j-simple"      % Version.slf4j
}


package com.solidfire.apigen


import java.io.File

import org.slf4j.LoggerFactory
import util.{ IOUtils, JsonUtils, ValidatorUtils }

sealed class ApiMerge

object ApiMerge extends IOUtils with JsonUtils with ValidatorUtils {

  def main( args: Array[String] ): Unit = {
    generate( "element", "element.json" )
    generate( "element-node", "element-node.json" )
  }

  val log = LoggerFactory.getLogger( classOf[ApiMerge] )

  def generate( description: String, output: String ): Unit = {

    val file = new File( output )

    writeToFile( file, mergeApi( description ) )

    validateApi( file )
  }

  def mergeApi( description: String ): String = {
    val files = loadJsonDescriptors( description )
    val allJson = parseAll( files )
    val merged = mergeJson( allJson )
    pretty( merged )
  }
}
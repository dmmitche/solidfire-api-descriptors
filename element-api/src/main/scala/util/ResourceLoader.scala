package util

import java.io._

import com.solidfire.apigen.ApiMerge._
import com.solidfire.jsvcgen.codegen.{ CliConfig, Validator }
import com.solidfire.jsvcgen.loader.JsvcgenDescription
import com.solidfire.jsvcgen.model.ServiceDefinition
import org.json4s._
import org.json4s.jackson.JsonMethods
import org.json4s.jackson.JsonMethods._

import scala.io.Source


/**
 * Created by Jason Ryan Womack on 7/9/15.
 */
trait IOUtils {

  def loadJsonDescriptors( resourceDir: String ): List[File] = {
    val dir = new java.io.File( this.getClass.getResource( s"/$resourceDir" ).getPath )

    def customSortOrder(file1: File, file2: File): Boolean = {
      (file1.getName,file2.getName) match {
        case (f1, f2) if f1.equalsIgnoreCase("ElementApi.json") => true
        case (f1, f2) if f2.equalsIgnoreCase("ElementApi.json") => false
        case (f1, f2) => f1.compareToIgnoreCase(f2) < 0
      }
    }

    dir.listFiles.filter( _.getName.endsWith( ".json" ) ).toList.sortWith(customSortOrder)
  }

  def writeToFile( file: File, s: String ): Unit = {
    val pw = new java.io.PrintWriter( file )
    try pw.write( s ) finally pw.close( )
  }
}


trait JsonUtils {

  def parseAll( files: List[File] ): List[JValue] = {
    files.map( file => parse( file ) )
  }

  def mergeJson( values: List[JValue] ): JValue = {
    values.foldLeft( JNothing: JValue )( ( a, b ) => a merge b )
  }

  def loadService( descriptionFile: File ): ServiceDefinition = {
    JsvcgenDescription.load( parse( Source.fromFile( descriptionFile ).mkString ) )
  }

  def pretty( value: JValue ): String = {
    JsonMethods.pretty( value )
  }
}

trait ValidatorUtils {
  def validateApi( input: File ): Unit = {

    val config = CliConfig(
      description = input,
      generator = "validate",
      namespace = "com.solidfire.element.api"
    )

    val validator = new Validator( config )

    val service = loadService( config.description )

    validator.generate( service )
  }
}
